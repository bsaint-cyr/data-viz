/**
 * Sanitizes the names from the data in the "Player" column.
 *
 * Ensures each word in the name begins with an uppercase letter followed by lowercase letters.
 *
 * @param {object[]} data The dataset with unsanitized names
 * @returns {object[]} The dataset with properly capitalized names
 */
// eslint-disable-next-line space-before-function-paren
export function cleanNames(data) {
  // TODO: Clean the player name data
  data.map(d => {
    d.Player = d.Player.charAt(0) + d.Player.slice(1).toLowerCase()
  })

  return data
}

/**
 * Finds the names of the 5 players with the most lines in the play.
 *
 * @param {object[]} data The dataset containing all the lines of the play
 * @returns {string[]} The names of the top 5 players with most lines
 */
export function getTopPlayers (data) {
  // TODO: Find the five top players with the most lines in the play
  const numberOfLinesPerPlayer = new Map()

  data.forEach(({ Player: name }) => {
    const lineCount = numberOfLinesPerPlayer.has(name) ? numberOfLinesPerPlayer.get(name) + 1 : 1
    numberOfLinesPerPlayer.set(name, lineCount)
  })

  const TOP = 5
  const topPlayers = Array.from(numberOfLinesPerPlayer
    .entries())
    .sort(([key1, value1], [key2, value2]) => value2 - value1)
    .slice(0, TOP)
    .map(([key, value]) => key)
  return topPlayers
}

/**
 * Transforms the data by nesting it, grouping by act and then by player, indicating the line count
 * for each player in each act.
 *
 * The resulting data structure ressembles the following :
 *
 * [
 *  { Act : ___,
 *    Players : [
 *     {
 *       Player : ___,
 *       Count : ___
 *     }, ...
 *    ]
 *  }, ...
 * ]
 *
 * The number of the act (starting at 1) follows the 'Act' key. The name of the player follows the
 * 'Player' key. The number of lines that player has in that act follows the 'Count' key.
 *
 * @param {object[]} data The dataset
 * @returns {object[]} The nested data set grouping the line count by player and by act
 */
export function summarizeLines (data) {
  // TODO : Generate the data structure as defined above
  const numActs = 5
  const summary = []

  for (let i = 0; i < numActs; i++) {
    const currentActLines = data.filter(({ Act }) => Act - 1 === i)
    summary[i] = { Act: i + 1, Players: [] }

    currentActLines.forEach(({ Player: name }) => {
      const indexOfPlayer = summary[i].Players.findIndex(({ Player }) => Player === name)

      if (indexOfPlayer !== -1) {
        summary[i].Players[indexOfPlayer].Count++
      } else {
        summary[i].Players.push({ Player: name, Count: 1 })
      }
    })
  }

  return summary
}

/**
 * For each act, replaces the players not in the top 5 with a player named 'Other',
 * whose line count corresponds to the sum of lines uttered in the act by players other
 * than the top 5 players.
 *
 * @param {object[]} data The dataset containing the count of lines of all players
 * @param {string[]} top The names of the top 5 players with the most lines in the play
 * @returns {object[]} The dataset with players not in the top 5 summarized as 'Other'
 */
export function replaceOthers (data, top) {
  // TODO : For each act, sum the lines uttered by players not in the top 5 for the play
  // and replace these players in the data structure by a player with name 'Other' and
  // a line count corresponding to the sum of lines
  const others = 'Other'
  let othersLineSum = 0

  data.forEach(({ Players }) => {
    let index = Players.length
    while (index--) {
      if (!top.includes(Players[index].Player)) {
        othersLineSum += Players[index].Count
        Players.splice(index, 1)
      }
    }
    Players.push({ Player: others, Count: othersLineSum })
    othersLineSum = 0
  })

  return data
}
