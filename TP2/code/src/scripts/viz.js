// import d3 from "d3"

/**
 * Sets the domain and range of the X scale.
 *
 * @param {*} scale The x scale
 * @param {object[]} data The data to be used
 * @param {number} width The width of the graph
 */
export function updateGroupXScale (scale, data, width) {
  return scale.domain([...Array(data.length).keys()].map(i => i + 1)).range([0, width])
}

/**
 * Sets the domain and range of the Y scale.
 *
 * @param {*} scale The Y scale
 * @param {object[]} data The data to be used
 * @param {number} height The height of the graph
 */
export function updateYScale (scale, data, height) {
  return scale.domain([0, d3.max(data, function(d) {
    return d.Players.reduce((maxLineCount, { Count }) => Count > maxLineCount ? Count : maxLineCount, 0)
  })
  ]).range([height, 0])
}

/**
 * Creates the groups for the grouped bar chart and appends them to the graph.
 * Each group corresponds to an act.
 *
 * @param {object[]} data The data to be used
 * @param {*} x The graph's x scale
 */
export function createGroups (data, x) {
  console.log(data)
  // TODO : Create the groups
  d3.select('#graph-g')
    .selectAll('g')
    .filter(function() {
      !(this.classList.contains('x axis') || this.classList.contains('y axis'))
    })
    .data(data)
    .enter()
    .append('g')
    .attr('class', 'act-group')
    .attr('transform', function(d, i) {
      return `translate(${x(i + 1)}, 0)`
    })
}


/**
 * Draws the bars inside the groups
 *
 * @param {*} y The graph's y scale
 * @param {*} xSubgroup The x scale to use to position the rectangles in the groups
 * @param {string[]} players The names of the players, each corresponding to a bar in each group
 * @param {number} height The height of the graph
 * @param {*} color The color scale for the bars
 * @param {*} tip The tooltip to show when each bar is hovered and hide when it's not
 */
export function drawBars (y, xSubgroup, players, height, color, tip) {
  players.forEach(name =>{
    d3.select('#graph-g')
      .selectAll('.act-group')
      .append('rect')
      .attr('x', function(d) {
        return xSubgroup(name)
      })
      .attr('y', function(d) {
        let playerData = d.Players.find(player => player.Player === name)
        if(playerData != undefined) {
          return y(playerData.Count)
        } else {
          return 0
        }
      })
      .attr('height', function(d) {
        let playerData = d.Players.find(player => player.Player === name)
        if(playerData != undefined) {
          return height - y(playerData.Count)
        } else {
          return 0
        }
      })
      .attr('width', xSubgroup.bandwidth())
      .attr('fill', function(d) {
        return color(name)
      })
      .on('mouseover', function(d) {
        tip.show({ 
          Act: d.Act, 
          Player: d.Players.find(({Player}) => Player === name)
        }, this)
      })
      .on('mouseout', tip.hide)
  })
}
