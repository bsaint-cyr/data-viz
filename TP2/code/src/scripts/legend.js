/**
 * Draws a legend in the area at the bottom of the screen, corresponding to the bars' colors
 *
 * @param {string[]} data The data to be used to draw the legend elements
 * @param {*} color The color scale used throughout the visualisation
 */
export function draw (data, color) {
  // TODO : Generate the legend in the div with class "legend". Each SVG rectangle
  // should have a width and height set to 15.
  // Tip : Append one div per legend element using class "legend-element".
  const width = 15
  const height = 15
  const padding = 5
  data.forEach(name => {
    const element = d3.select('.legend')
    element.append('svg')
      .attr('height', height)
      .attr('width', width + padding)
      .append('rect')
      .attr('fill', function (d) {
        return color(name)
      })
      .attr('width', width)
      .attr('height', height)
    element.append('text').text(name).attr('x', width + padding).attr('y', height)
    element.append('div').attr('class', 'legend-element')
  })
}
