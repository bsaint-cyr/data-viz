/**
 * Gets the names of the neighborhoods.
 *
 * @param {object[]} data The data to analyze
 * @returns {string[]} The names of the neighorhoods in the data set
 */
export function getNeighborhoodNames (data) {
  // TODO: Return the neihborhood names
  return data.map(({Arrond_Nom}) => Arrond_Nom)
}

/**
 * Filters the data by the given years.
 *
 * @param {object[]} data The data to filter
 * @param {number} start The start year (inclusive)
 * @param {number} end The end year (inclusive)
 * @returns {object[]} The filtered data
 */
export function filterYears (data, start, end) {
  // TODO : Filter the data by years
  return data.filter(({ Date_Plantation: date }) =>  {
    const year = date.getFullYear()
    return start <= year && year <= end
  })
}

function groupBy (xs, fn) {
  return xs.reduce((rv, x) => {
    (rv[fn(x)] = rv[fn(x)] || []).push(x);
    return rv;
  }, {})
}

function countBy (xs, fn) {
  return xs.reduce((rv, x) => {
    rv[fn(x)] = rv[fn(x)] + 1 || 1
    return rv
  }, {})
}

/**
 * Summarizes how any trees were planted each year in each neighborhood.
 *
 * @param {object[]} data The data set to use
 * @returns {object[]} A table of objects with keys 'Arrond_Nom', 'Plantation_Year' and 'Counts', containing
 * the name of the neighborhood, the year and the number of trees that were planted
 */
export function summarizeYearlyCounts (data) {
  // TODO : Construct the required data table
  // group by name and then by year

  let summary = groupBy(data, ({Arrond_Nom}) => Arrond_Nom)
  Object.keys(summary).forEach(name => {
    summary[name] = countBy(summary[name], ({Date_Plantation: date}) => date.getFullYear())
  })
  // convert map to table
  let table = []
  Object.keys(summary).forEach(Arrond_Nom => {
    Object.keys(summary[Arrond_Nom]).forEach(Plantation_Year => {
      const Counts = summary[Arrond_Nom][Plantation_Year]
      table.push({Arrond_Nom, Plantation_Year, Counts})
    })
  })
  return table
}

/**
 * For the heat map, fills empty values with zeros where a year is missing for a neighborhood because
 * no trees were planted or the data was not entered that year.
 *
 * @param {object[]} data The datas set to process
 * @param {string[]} neighborhoods The names of the neighborhoods
 * @param {number} start The start year (inclusive)
 * @param {number} end The end year (inclusive)
 * @param {Function} range A utilitary function that could be useful to get the range of years
 * @returns {object[]} The data set with a new object for missing year and neighborhood combinations,
 * where the values for 'Counts' is 0
 */
export function fillMissingData (data, neighborhoods, start, end, range) {
  // TODO : Find missing data and fill with 0
  const years = range(start, end)
  neighborhoods.forEach(neighborhood => {
    const containsNeighborhood = data.some(({Arrond_Nom}) => Arrond_Nom === neighborhood)
    years.forEach(year => {
      year = year.toString()
      const defaultValue = {
        Arrond_Nom: neighborhood, 
        Plantation_Year: year, 
        Counts: 0
      }

      if (!containsNeighborhood) {
        data.push(defaultValue)
      } else {
        const containsYear = data.some(({Arrond_Nom, Plantation_Year}) => {
          return Arrond_Nom === neighborhood && Plantation_Year == year
        })
        if(!containsYear) {
          data.push(defaultValue)
        }
      }
    })
  })
  console.log(data)
  return data
}
