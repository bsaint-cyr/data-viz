/**
 * Sets up an event handler for when the mouse enters and leaves the squares
 * in the heatmap. When the square is hovered, it enters the "selected" state.
 *
 * The tick labels for the year and neighborhood corresponding to the square appear
 * in bold.
 *
 * @param {*} xScale The xScale to be used when placing the text in the square
 * @param {*} yScale The yScale to be used when placing the text in the square
 * @param {Function} rectSelected The function to call to set the mode to "selected" on the square
 * @param {Function} rectUnselected The function to call to remove "selected" mode from the square
 * @param {Function} selectTicks The function to call to set the mode to "selected" on the ticks
 * @param {Function} unselectTicks The function to call to remove "selected" mode from the ticks
 */
export function setRectHandler (xScale, yScale, rectSelected, rectUnselected, selectTicks, unselectTicks) {
  // TODO : Select the squares and set their event handlers
  var groups = d3.select('#graph-g').selectAll('rect')
  groups.on('mouseover', function() {
    var wtv = d3.select(this)
    wtv.append('text').attr('id', 'selected')
    rectSelected(wtv, xScale, yScale)
    selectTicks(wtv._groups[0][0].__data__.Arrond_Nom, wtv._groups[0][0].__data__.Plantation_Year)
  })
  groups.on('mouseout', function() {
    rectUnselected(d3.select(this))
    unselectTicks()
  })
}

/**
 * The function to be called when one or many rectangles are in "selected" state,
 * meaning they are being hovered
 *
 * The text representing the number of trees associated to the rectangle
 * is displayed in the center of the rectangle and their opacity is lowered to 75%.
 *
 * @param {*} element The selection of rectangles in "selected" state
 * @param {*} xScale The xScale to be used when placing the text in the square
 * @param {*} yScale The yScale to be used when placing the text in the square
 */
export function rectSelected (element, xScale, yScale) {
  // TODO : Display the number of trees on the selected element
  // Make sure the nimber is centered. If there are 1000 or more
  // trees, display the text in white so it contrasts with the background.
  var nbOfTrees = d3.select(element._groups[0][0])
  nbOfTrees.attr('opacity', '0.75')
  if (element._groups[0][0].__data__.Counts < 1000) {
    nbOfTrees.select('#selected')
    .attr('x', xScale.bandwidth()/2)
    .attr('y', yScale.bandwidth())
    .attr('text-anchor', 'middle') 
    .text(element._groups[0][0].__data__.Counts)
    .attr("fill", "black")
  }
  else {
    nbOfTrees.select('#selected')
    .attr('x', xScale.bandwidth()/2)
    .attr('y', yScale.bandwidth())
    .attr('text-anchor', 'middle') 
    .text(element._groups[0][0].__data__.Counts)
    .attr("fill", "white")
  }
}

/**
 * The function to be called when the rectangle or group
 * of rectangles is no longer in "selected state".
 *
 * The text indicating the number of trees is removed and
 * the opacity returns to 100%.
 *
 * @param {*} element The selection of rectangles in "selected" state
 */
export function rectUnselected (element) {
  // TODO : Unselect the element
  var wtv = d3.select(element._groups[0][0])
  wtv.select('#selected').remove()
  wtv.attr('opacity', '1')
}

/**
 * Makes the font weight of the ticks texts with the given name and year bold.
 *
 * @param {string} name The name of the neighborhood associated with the tick text to make bold
 * @param {number} year The year associated with the tick text to make bold
 */
export function selectTicks (name, year) {
  // TODO : Make the ticks bold
  d3.select('.y.axis')
    .selectAll('text')
    .filter(function() {
      return this.innerHTML == name
    })
    .attr('font-weight', 'bold')
  
  d3.select('.x.axis')
    .selectAll('text')
    .filter(function() {
      return this.innerHTML == year
    })
    .attr('font-weight', 'bold')

}

/**
 * Returns the font weight of all ticks to normal.
 */
export function unselectTicks () {
  // TODO : Unselect the ticks
  d3.select('.y.axis')
    .selectAll('text')
    .attr('font-weight', 'normal')

    d3.select('.x.axis')
    .selectAll('text')
    .attr('font-weight', 'normal')

}
