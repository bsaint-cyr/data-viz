/**
 * Positions the x axis label and y axis label.
 *
 * @param {*} g The d3 Selection of the graph's g SVG element
 * @param {number} width The width of the graph
 * @param {number} height The height of the graph
 */
export function positionLabels (g, width, height) {
  // TODO : Position axis labels
  g.select('.x.axis-text')
    .attr('x', width / 2)
    .attr('y', height + 50)

  g.select('.y.axis-text')
    .attr('x', -50)
    .attr('y', height / 2)
}

/**
 * Draws the circles on the graph.
 *
 * @param {object} data The data to bind to
 * @param {*} rScale The scale for the circles' radius
 * @param {*} colorScale The scale for the circles' color
 */
export function drawCircles (data, rScale, colorScale) {
  // TODO : Draw the bubble chart's circles
  // Each circle's size depends on its population
  // and each circle's color depends on its continent.
  // The fill opacity of each circle is 70%
  // The outline of the circles is white
  d3.select('#graph-g')
    .selectAll('.bubbles')
    .data(data)
    .enter()
    .append('circle')
    .attr('class', 'bubbles')
    .attr('r', function (d, i) {
      return rScale(d.Population)
    })
    .style('fill', function (d, i) {
      return colorScale(d.Continent)
    })
    .style('fill-opacity', '70%')
    .style('stroke', 'white')
}

/**
 * Sets up the hover event handler. The tooltip should show on on hover.
 *
 * @param {*} tip The tooltip
 */
export function setCircleHoverHandler (tip) {
  // TODO : Set hover handler. The tooltip shows on
  // hover and the opacity goes up to 100% (from 70%)
  d3.selectAll('.bubbles')
    .on('mouseover', function (d) {
      tip.show(d, this)
      d3.select(this)
        .style('fill-opacity', '100%')
    })
    .on('mouseout', function () {
      tip.hide()
      d3.select(this)
        .style('fill-opacity', '70%')
    })
}

/**
 * Updates the position of the circles based on their bound data. The position
 * transitions gradually.
 *
 * @param {*} xScale The x scale used to position the circles
 * @param {*} yScale The y scale used to position the circles
 * @param {number} transitionDuration The duration of the transition
 */
export function moveCircles (xScale, yScale, transitionDuration) {
  // TODO : Set up the transition and place the circle centers
  // in x and y according to their GDP and CO2 respectively
  d3.selectAll('circle')
    .transition()
    .duration(transitionDuration)
    .attr('cx', function (d) {
      return xScale(d.GDP)
    })
    .attr('cy', function (d) {
      return yScale(d.CO2)
    })
}

/**
 * Update the title of the graph.
 *
 * @param {number} year The currently displayed year
 */
export function setTitleText (year) {
  // TODO : Set the title
  d3.select('.title').text(`Data for year: ${year}`)
}
