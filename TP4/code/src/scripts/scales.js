/**
 * Defines the scale to use for the circle markers' radius.
 *
 * The radius of the circle is linearly proportinal to the population of the given country.
 *
 * The radius is a value defined in the interval [5, 20].
 *
 * @param {object} data The data to be displayed
 * @returns {*} The linear scale used to determine the radius
 */
export function setRadiusScale (data) {
  // TODO : Set scale
  const fullData = [...data['2000'], ...data['2015']]
  const domain = d3.extent(fullData, ({ Population }) => Population)
  const rScale = d3.scaleLinear().domain(domain).range([5, 20])
  return rScale
}

/**
 * Defines the color scale used to determine the color of the circle markers.
 *
 * The color of each circle is determined based on the continent of the country it represents.
 *
 * The possible colors are determined by the scheme d3.schemeCategory10.
 *
 * @param {object} data The data to be displayed
 * @returns {*} The ordinal scale used to determine the color
 */
export function setColorScale (data) {
  // TODO : Set scale
  const domain = new Set([...data['2000'], ...data['2015']].map(({ Continent }) => Continent))
  const cScale = d3.scaleOrdinal().domain(Array.from(domain).sort()).range(d3.schemeCategory10)
  return cScale
}

/**
 * Defines the log scale used to position the center of the circles in X.
 *
 * @param {number} width The width of the graph
 * @param {object} data The data to be used
 * @returns {*} The linear scale in X
 */
export function setXScale (width, data) {
  // TODO : Set scale
  const xData = [...data['2000'], ...data['2015']]
  const domain = d3.extent(xData, ({ GDP }) => GDP)
  return d3.scaleLog()
    .domain(domain)
    .range([0, width])
}

/**
 * Defines the log scale used to position the center of the circles in Y.
 *
 * @param {number} height The height of the graph
 * @param {object} data The data to be used
 * @returns {*} The linear scale in Y
 */
export function setYScale (height, data) {
  // TODO : Set scale
  const yData = [...data['2000'], ...data['2015']]
  const domain = d3.extent(yData, ({ CO2 }) => CO2)
  return d3.scaleLog()
    .domain(domain)
    .range([height, 0])
}
