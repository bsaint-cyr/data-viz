import d3Legend from 'd3-svg-legend'

/**
 * Draws the color legend.
 *
 * @param {*} colorScale The color scale used for the legend
 * @param {*} g The d3 Selection of the SVG g elemnt containing the legend
 */
export function drawLegend (colorScale, g) {
  // TODO : Generate the legend
  // For help, see : https://d3-legend.susielu.com/
  const legend = d3Legend.legendColor()
    .shape('circle')
    .title('Légende')
    .scale(colorScale)

  g.append('g')
    .attr('transform', 'translate(50,120)')
    .attr('font-family', 'Open Sans Condensed')
    .attr('font-size', 18)
    .call(legend)
}
